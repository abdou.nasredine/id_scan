<?php

namespace Database\Seeders;

use App\Enums\UserNature;
use App\Models\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Client::create([
            'name' => 'Nasreddine ABDELLI',
            'qr_code' => 'ABC123',
            'nature' => UserNature::STUDENT,
        ]);
    }
}
