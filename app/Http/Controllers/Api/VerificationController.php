<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;

class VerificationController extends Controller
{

    private function jsonRespons($message = '', $data = [], $status = 200)
    {
        return response()->json(['message' => $message, 'body' => $data], $status);
    }

    public function verfiy($qr_code)
    {

        $client = Client::query()->firstWhere('qr_code', $qr_code);

        if (!$client) return $this->jsonRespons('User not found', [], 404);

        // todo: check if user type & validity

        return $this->jsonRespons('User found', ['user' => $client]);

    }
}
