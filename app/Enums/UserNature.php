<?php

namespace App\Enums;

enum UserNature : string
{
    case STUDENT = 'STUDENT';
    case VISITOR = 'VISITOR';
    case EMPLOYEE = 'EMPLOYEE';
}
